#!/bin/bash

cd tmp && \
curl -O https://wordpress.org/latest.tar.gz && \
tar -xzf latest.tar.gz && \
cp /tmp/wordpress/wp-config-sample.php /tmp/wordpress/wp-config.php && \
mkdir /tmp/wordpress/wp-content/upgrade && \
cp -a /tmp/wordpress/. /var/www/wordpress && \
chown -R www-data:www-data /var/www/wordpress && \
find /var/www/wordpress/ -type d -exec chmod 750 {} \; && \
find /var/www/wordpress/ -type f -exec chmod 640 {} \;
