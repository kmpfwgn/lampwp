FROM ubuntu:18.04

RUN apt update -y && export DEBIAN_FRONTEND=noninteractive && \
	apt install -y tzdata && \ 
	ln -fs /usr/share/zoneinfo/Europe/Minsk /etc/localtime && \
	dpkg-reconfigure --frontend noninteractive tzdata

RUN apt install -y apache2 mysql-server curl php7.0 libapache2-mod-php php-mysql php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip

COPY ./wordpress.conf /etc/apache2/sites-available/

COPY ./configurewp.sh /opt
COPY ./initdb.sql /opt

RUN service mysql start && mysql < /opt/initdb.sql
RUN /bin/bash /opt/configurewp.sh
COPY ./wp-config.php /var/www/wordpress/

RUN a2enmod rewrite && a2dissite 000-default.conf && a2ensite wordpress.conf

EXPOSE 80

CMD service mysql start; apachectl -D FOREGROUND
